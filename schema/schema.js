const graphql = require('graphql');
const _ = require('lodash');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema

} = graphql;

// hardcoded users to fetch
const users = [
    { id: '23', firstName: 'Bill', age: 20 },
    { id: '47', firstName: 'Samantha', age: 21 }
];

const UserType = new GraphQLObjectType({
    // name is a REQUIRED property (think of this as table name) 
    name: 'User',
    //fields is a REQUIRED object !important  (think of these as columns)
    fields: {
        id: { type: GraphQLString },
        firstName: { type: GraphQLString },
        age: { type: GraphQLInt }
    }
});

// Serves as the ENTRY POINT to the data, required by GraphQL
// This instructs GraphQL how to find the data defined by the
// schema. (i.e. If an id is provided for a user, GraphQL will
// return a UserType)
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        user: {
            type: UserType,
            args: { id: { type: GraphQLString } },
            // resolve is the actual function used to retrieve the data
            // from the datastore/db !important
            resolve(parentValue, args) {
                return _.find(users, { id: args.id });
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery
});